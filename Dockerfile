FROM node:latest

RUN apt-get update && \
    apt-get install -y brotli

RUN npm i -g npm@latest elm@latest elm-test@latest parcel-bundler@latest uglify-js@latest elm-minify@latest elm-pages@latest
