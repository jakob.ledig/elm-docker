# elm-docker

Plain and simple docker image to build elm projects in a pipeline or whatever. Not the smallest image in size, but it
may get the job done.

## Installed bibs

- elm
- elm-test